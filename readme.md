## Hybrid is a Conky theme that combines modern and classic style.

| Theme | Distro Logos |
| ----------- | ----------- |
| ![Conky Image](/screenshots/screenshot.png) | ![Conky Image](/screenshots/distro-logos.png) |


## Change Log

- v1.6.4
    - added handler to prevent issue that's causing circle draw to breaks when hwmon not found due to the latest conky change behavior

- v1.6.3
    - undo circlewriting for storage usage percentage
    - updated screenshots 

- v1.6.2
    - fixed lua script to auto detect the correct hwmon folder as the folder name keep changing (hwmon3, hwmon4, hwmon5 etc) each time the machine restart. usage of incorrect hwmon folder name in the command will cause lua rings failed to render 

- v1.6.1
    - updated manjaro icons
    - fixed lua script to make it works with conky-lua-nv 1.21.2-1

- v1.6.0
    - added install.sh script for package installation. I'll be using deploy.sh for package deployment
    - changed accent color from 4285F4 to 3458eb
    - removed g_alt_colour 
    - changed font to NotoSans
    - removed ubuntu font from package
    - implemented circlewriting for storage (still thinking about this)
    - adjusted storage radius
    - adjusted clock position
    - fixed hour clock, draw ring indicator with 12 hours value while text displaying in 24 hours format
    - removed light version

- v1.5.7
    - adjusted clock ring
    - hide time second text

- v1.5.6
    - changed font to Ubuntu
    - set accent bg_alpha to 1.0 to increase readability
    - changed rings bg_alpha from [0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9] to [0.2,0.3,0.4,0.5,0.3,0.5,0.5,0.3]
    - changed text_indicator alpha value
    - fixed text_indicator coordinate
    - updated deployment script
    - renamed .config extension to .conf
    - turned off line sketches by default

- v1.5.5
    - fixed script backward compatibility on older version lua library (tested on liblua5.1-0 binded with conky use by Linux Mint 20)
    - added kali linux logo

- v1.5.4
    - minor updates. missed out something yesterday

- v1.5.3
    - added 12 distro logos
    - added text indicator
    - added line sketches background (with on/off toggle)
    - rearranged texts
    - code refactoring

- v1.5.2
    - splitted text settings from rings settings
    - adjusted cpu thread rings

- v1.5.1
    - changed time to 24 hours format
    - fixed level watch, should check with percentage instead of value
    - added clock rings

- v1.5
    - added battery
    - added swap (I've omitted swap from my system, that's why it's 0 in screenshot)
    - added ring for time second tick, just for eye candy
    - changed cpu, gpu and ram ring angle
    - moved disk and ram info to right panel
    - code refactoring

- v1.4 
    - Fix not synchronize data between ring usage animation vs percentage text render from conky
    - Simplify and standardize usage level watch
    - Render text display on rings via lua script instead of conky

- v1.3
    - Render distro logo

- v1.2
    - Set smaller ring radius and adjust position

- v1.1
    - Removed un-needed methods from the script

- v1.0
    - Integrate rings into my existing conkyrc


## Dependencies
- **Arch based distro package**
    - conky-lua-nv (with nvidia object) **OR** conky-cairo
 
- **Debian based distro package**
    - conky-all
 
- **Fonts**
    - NotoSans (default)
    - Play (optional - this theme plays well with Play font. Need to change font name in .conf and .lua file)
    - OpenLogos (optional - only in case you want to use logo from OpenLogos)


## Installation
- Fonts
    - Install fonts like normal.

- Download the project and extract it.
    - Inside the project search for install.sh file (**for v1.6.0 and above, please use install.sh. DO NOT use deploy.sh as it is use for packaging the files into tarballs**)
    - Set file install.sh to be executable
    - Execute the script in Konsole / Terminal

            $ ./install.sh


## To use
- Open Konsole / Terminal and run
            $ conky -c ~/.config/conky/hybrid/hybrid.conf &


- Alternatively
    - Create symlink from your config
    - Paste it to home folder
    - Rename symlink to .conkyrc
    - In terminal run conky

            $ conky &


## Auto Start
- Inside startup folder search for hybrid-startup.sh file
- Set hybrid-startup.sh to be executable
- Change the home path to your home path, seems like I can't use ~ at startup script
- Add hybrid-startup.sh to auto start program
- You can adjust sleep time in the script accordingly


## Customizations
- **Line sketches toggle**
    - Line sketches background can be turn on or off from conky config file:

            -- to turn on
            lua_draw_hook_pre = 'conky_main true'

            -- to turn off
            lua_draw_hook_pre = 'conky_main false'

 
- **Distro Logo**
    - In hybrid.config, you can use distro logo from OpenLogos or alternatively you can use png image file.
        - OpenLogos example:

                ${goto 250}${color2}${font Play:bold:size=16}${alignc}${execi 300 lsb_release -d | cut -c14-} ${color}${font OpenLogos:size=24}t

        - Png image example:

                ${voffset -930}${image ~/.config/conky/hybrid/images/distro-1a.png -p 475,16 -s 25x25}
                ${goto 250}${color2}${font Play:bold:size=16}${alignc}${execi 300 lsb_release -d | cut -c14-} ${color}

        - **Update 09 June 2020** 
            - Distro logo is now render from Lua script instead of Conky. To change open hybrid-rings.lua file and search for draw_logo() function, don't forget to change home_dir value. Edit the line below:

                    local imagefile = home_dir .. "/.config/conky/hybrid/images/distro-1a.png"

        - **Logos available**
                1. Manjaro
                2. Linux Mint
                3. Arch Linux
                4. Ubuntu
                5. MX Linux
                6. Debian
                7. Elementary OS
                8. Pop! OS
                9. Solus
                10. Fedora
                11. Zorin
                12. PCLinuxOS
            13. Kali Linux
 

- **Network interface** 
    - In hybrid.config, you need to change network card interface. Replace enp7s0f1 and wlp0s20f3 with yours. If you only have a single interface you can commented out the second network interface. However, this is optional.

            #${if_existing /proc/net/route wlp0s20f3}\
            #${goto 250}${font Play:style=Bold:size=8}Internal IP - wlp0s20f3 ${font Play:size=8}$# {alignr}${addr wlp0s20f3}
            #${goto 250}    ${font Play:style=Bold:size=8}Download
            #${goto 250}        ${color2}${font Play:size=8}Speed ${alignr} ${downspeed # wlp0s20f3}
            #${goto 250}        ${color}Total ${alignr} ${totaldown wlp0s20f3}
            #${goto 250}    ${font Play:style=Bold:size=8}Upload
            #${goto 250}        ${color2}${font Play:size=8}Speed ${alignr} ${upspeed # wlp0s20f3}
            #${goto 250}        ${color}Total ${alignr} ${totalup wlp0s20f3}
            #${endif}\

    - To check you network interface use command:

            $ ip link show


- **Number of CPUs and threads**
    - This theme support 6 CPUs and 12 threads by default. In case if you're having less number of CPUs and threads, please comment the extra CPUs and threads in Hybrid-rings.lia script. This is not needed in the old conky lua version. However the behavior has change in the newer version.

            settings_table = {
                -- cpu core temperature
                
                ...
                
                -- {
                --     name='platform',
                --     arg='coretemp.0/hwmon/hwmon_x temp 4',
                --     max=110,
                --     bg_colour=0xa8a8a8,
                --     bg_alpha=0.5,
                --     fg_colour=0x3458eb,
                --     fg_alpha=1.0,
                --     x=140, y=140,
                --     radius=100,
                --     thickness=4,
                --     start_angle=0,
                --     end_angle=270,
                --     text_id=3
                -- },
                ..
                -- cpu usage (text height=16, ring total height=230)
                
                ...
                
                -- {
                --     name='cpu',
                --     arg='cpu7',
                --     max=100,
                --     bg_colour=0xa8a8a8,
                --     bg_alpha=0.5,
                --     fg_colour=0x3458eb,
                --     fg_alpha=1.0,
                --     x=140, y=370,
                --     radius=66,
                --     thickness=15,
                --     start_angle=0,
                --     end_angle=90,
                --     text_id=25
                -- },



- **NVidia GPU**
    - This theme support NVidia GPU temperature by default. If your machine doesn't have one, you can comment the code below:

            -- {
            --     name='nvidia',
            --     arg='temp',
            --     max=110,
            --     bg_colour=0xa8a8a8,
            --     bg_alpha=0.9,
            --     fg_colour=0x4285F4,
            --     fg_alpha=0.8,
            --     x=140, y=370,
            --     radius=106,
            --     thickness=4,
            --     start_angle=0,
            --     end_angle=270,
            --     text_id=9
            -- },


- **CPUs**
    - Number of CPUs thread and Core: In hybrid-rings.lua, you can reduce number of CPUs and core according to you system. Search for settings_table and modified accordingly.
    It should be simple enough to understand. The outer ring on CPU represents CPU Core temperature.
    - To check you system CPUs and Cores, use command below:

            $ lscpu | egrep 'Model name|Socket|Thread|NUMA|CPU\(s\)'


- **Disk**
    - In hybrid-rings.lua, you can adjust number of partition according to your system. Search for settings_table and modified accordingly. The outer ring on storage represents ram, swap and battery usage.
 

- **Known Issues**
    - Storage circle overlapped on top of storage text. As a workaround I've set the storage text position so that it did not get overlap by storage circle.
    - ~~Sometimes cpu core temperature draw on rings and display text on conky are not in sync.~~


## Disclaimer
I am not the original creator of the conkyrc file (has been tweaked for my usage).
It was downloaded way back in 2009 and I've no information of the original creator. Credit should go to him/her.

hybrid-rings.lua / hybrid-light-rings.lua script is a modified and refactored version of mine. Script originally copied from conky-grapes theme's rings-v2_tpl which was created by **londonali1010** (2009), updated by **La-Manoue** (2016) and **popi** (2017).

setup_circle_text was copied and modified from circlewriting function created by **mrpeachy**.

I do not own any of the distro logos bundled with this script. Please inform me if in case any of the logo are not allowed to be share.
I will remove it as soon as possible.


## External Links
[Arch Wiki Conky Page](https://wiki.archlinux.org/title/Conky#Configuration)

[Conky Objects](http://conky.sourceforge.net/variables.html)

[Lua 5.4 Reference Manual](https://www.lua.org/manual/5.4/)

[Cairo Samples](https://cairographics.org/samples/)