#!/bin/bash

if [ -d ~/source-code/deploy/hybrid/ ]
then
    rm -R ~/source-code/deploy/hybrid/
    echo 'package deleted'
fi

rsync -IrW --stats --exclude={'.git','deploy.sh','readme.md','screenshots','bad-experiment','classic'} $(pwd) ~/source-code/deploy/
echo 'hybrid copied'

cd ~/source-code/deploy
echo 'go to deploy folder'

tar -czvf hybrid.tar.gz hybrid
echo 'package archived'