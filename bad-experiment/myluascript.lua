function conky_myscript_init(p_fontname, p_fontsize)
    gfontname = p_fontname
    gfontsize = p_fontsize

    return ""
end


function conky_myscript_destroy()
    gfontname = nil
    gfontsize = nil

    return ""
end


-- args: t, NotoSans, 16, 10, 10
function conky_get_header(p_charlogo, p_fontname, p_h1fontsize, p_h2fontsize, p_h3fontsize)
    local headerfont1 = string.format( "${font %s:bold:size=%s}", p_fontname, p_h1fontsize )
    local headerfont2 = string.format( "${font %s:bold:size=%s}", p_fontname, p_h2fontsize )
    local headerfont3 = string.format( "${font %s:size=%s}", p_fontname, p_h3fontsize )
    local datetimestring = "${alignc}${time %A %d %b %Y} ${time %I:%M %p}"

    local longstring = [[${color3}${hr}${color2}

<headerfont1>${alignc}${execi 300 lsb_release -d | cut -c14-} - ${font OpenLogos:size=24}<p_charlogo>
${voffset -16}<headerfont2>${alignc}${execi 300 lsb_release -c | cut -c11-} ${execi 300 lsb_release -r | cut -c9-}
<headerfont3>]]

    return string.format( f(longstring), p_charlogo, headerfont1, headerfont2, headerfont3 ) .. datetimestring
end


function conky_get_system(p_cputhreads, p_cpucores)
    local result = get_segment_header("SYSTEM")

    result = result .. conky_get_system_kernel()
    result = result .. conky_get_cpu_bars(p_cputhreads) .. "\n"
    result = result .. conky_get_system_ram()
    result = result .. conky_get_system_swap()
    result = result .. conky_get_cpu_cores_temperature(p_cpucores)

    if conky_get_nvidia_gpu_temperature() ~= "" then
        result = result .. conky_get_nvidia_gpu_temperature()
    end

    result = result .. conky_get_system_uptime()

    return result
end


function conky_get_system_kernel()
    return string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}Name ${font <gfontname>:size=<gfontsize>}${alignr}${sysname} kernel ${kernel}
]]), gfontname, gfontsize) 
end


function conky_get_system_ram()
    return string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}RAM ${font <gfontname>:size=<gfontsize>}${alignr}${mem} / ${memmax} (${memperc}${execi 300 echo -e '\x25'})
]]), gfontname, gfontsize )
end


function conky_get_system_swap()
    return string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}SWAP ${font <gfontname>:size=<gfontsize>}${alignr}${swap} / ${swapmax} (${swapperc}${execi 300 echo -e '\x25'})
]]), gfontname, gfontsize )
end


function conky_get_nvidia_gpu_temperature()
    local nvidia_used = toboolean(conky_parse("${if_match \"${nvidia temp}\" != \"\"}true${else}false${endif}"))
    local gpu_name = tostring(conky_parse("${execi 300 nvidia-smi -i 0 --query-gpu=gpu_name --format=csv,noheader,nounits}"))

    if not nvidia_used then
        return ""
    end

    return string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}<gpu_name> ${font <gfontname>:size=<gfontsize>}${alignr}${nvidia temp} °C
]]), gfontname, gfontsize, gpu_name )
end


function conky_get_system_uptime()
    return string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}Uptime ${font <gfontname>:size=<gfontsize>}${alignr}${uptime_short}
]]), gfontname, gfontsize )
end


function conky_get_cpu_bars(p_cputhreads)
    p_cputhreads = tonumber(p_cputhreads)
    local rows = p_cputhreads / 2
    local result = ""
    local col1 = 1
    local col2 = 2
    
    -- \x25 == %
    if p_cputhreads > 1 then
        local longstring = [[
${font <gfontname>:style=Bold:size=<gfontsize>}CPU<displaynum1> ${font <gfontname>:size=<gfontsize>}${color3} ${cpubar cpu<col1> 13, 80}${color}
${voffset -18}${alignc 45}${cpu cpu<col1>}${execi 300 echo -e '\x25'}
${voffset -14}${alignr -5}${font <gfontname>:style=Bold:size=<gfontsize>}CPU<displaynum2> ${font <gfontname>:size=<gfontsize>}${color3} ${cpubar cpu<col2> 13, 80}${color}
${voffset -17}${alignc -85}${cpu cpu<col2>}${execi 300 echo -e '\x25'}
]]

        for row = 1, rows, 1 do
            local displaynum1 = string.format( "%02d", col1 )
            local displaynum2 = string.format( "%02d", col2 )

            result = result .. string.format( f(longstring), col1, displaynum1, col2, displaynum2, gfontname, gfontsize )
            col1 = col1 + 2
            col2 = col2 + 2
        end
    else
        local displaynum1 = string.format( "%02d", p_cputhreads )

        result = string.format( f([[
${font <gfontname>:style=Bold:size=<gfontsize>}TH <displaynum1> ${alignr -6}${font <gfontname>:size=<gfontsize>}${color3} ${cpubar cpu<p_cputhreads> 13, 190}${color}
${voffset -17}${alignc -30}${cpu cpu<p_cputhreads>}${execi 300 echo -e '\x25'}
]]), displaynum1, p_cputhreads, gfontname, gfontsize )
    end

    return result
end


function conky_get_cpu_cores_temperature(p_cpucores)
    p_cpucores = tonumber(p_cpucores)
    local rows = p_cpucores / 2
    local result = ""
    local col1 = 1
    local cpuid1 = col1 - 1
    local col2 = 2
    local cpuid2 = col2 - 1

    local longstring = [[
${font <gfontname>:style=Bold:size=<gfontsize>}Core <col1> ${font <gfontname>:size=<gfontsize>}                  ${exec sensors | grep 'Core <cpuid1>' | cut -c17-18} °C
${voffset -16}${alignr}${font <gfontname>:style=Bold:size=<gfontsize>}Core <col2> ${font <gfontname>:size=<gfontsize>}                  ${exec sensors | grep 'Core <cpuid2>' | cut -c17-18} °C
]]

    if p_cpucores > 1 then
        for row = 1, rows, 1 do
            result = result .. string.format( f(longstring), col1, cpuid2, col2, cpuid2, gfontname, gfontsize )
            
            col1 = col1 + 2
            col2 = col2 + 2

            cpuid1 = col1 - 1
            cpuid2 = col2 - 1
        end    
    else
        result = string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}CPU ${font <gfontname>:size=<gfontsize>}${alignr}${acpitemp} °C
]]), gfontname, gfontsize )
    end

    return result
end


function conky_get_storage(p_storages)
    p_storages = trimarraystring(p_storages)

    local storages = split(p_storages, "[^,]*") -- everything except commas
    local result = get_segment_header("STORAGE") --.. type(storages) .. ' ' .. table.maxn(storages) .. p_storages

    local fontstring = string.format(f("${font <gfontname>:size=<gfontsize>}"), gfontname, gfontsize)
    local longstring = [[${color3}${fs_bar 12 <storage>}${color}
${voffset -18}  <storage> 
${alignr}${fs_used <storage>} / ${fs_size <storage>} (${fs_used_perc <storage>}${execi 300 echo -e '\x25'}) 
]]

    for s = 1, #storages do
        local storage = tostring(storages[s])
        result = result .. string.format( f(longstring), storage )
    end

    return fontstring .. result
end


function conky_get_network(p_net_interfaces)
    p_net_interfaces = trimarraystring(p_net_interfaces)

    local interfaces = split(p_net_interfaces, "[^,]*")
    local interface_is_up = false
    local result = get_segment_header("NETWORK")

    for i = 1, #interfaces do
        local interface = tostring(interfaces[i])
        interface_is_up = toboolean(conky_parse(string.format( "${if_existing /proc/net/route %s}true${else}false${endif}", interface )))
        
        if interface_is_up then break end
    end

    if not interface_is_up then
        result = result .. "N/A\n"
        return result
    end

    -- this is for external IP
    -- ${font NotoSans:style=Bold:size=8}External IP ${font NotoSans:size=8}${alignr}${execi 14400 wget http://canyouseeme.org/ -O - 2>/dev/null | awk '/name="IP"/{if (gsub(/[^0-9.]+/,"")) {print}} '}

    local longstring = [[${if_existing /proc/net/route <interface>}${font <gfontname>:style=Bold:size=<gfontsize>}Internal IP - <interface> ${font <gfontname>:size=<gfontsize>}${alignr}${addr <interface>}
    ${font <gfontname>:style=Bold:size=<gfontsize>}Download
    ${goto 70}${downspeedgraph <interface> 25,125 4285F4 224488}
        ${voffset -39}${font <gfontname>:size=<gfontsize>}${color2}Speed ${alignr} ${downspeed <interface>}
        ${color}Total ${alignr} ${totaldown <interface>}
    ${font <gfontname>:style=Bold:size=<gfontsize>}Upload
    ${goto 70}${downspeedgraph <interface> 25,125 4285F4 224488}
        ${voffset -39}${font <gfontname>:size=<gfontsize>}${color2}Speed ${alignr} ${upspeed <interface>}
        ${color}Total ${alignr} ${totalup <interface>}${endif}
]]

    for i = 1, #interfaces do
        local interface = tostring(interfaces[i])
        interface_is_up = toboolean(conky_parse(string.format( "${if_existing /proc/net/route %s}true${else}false${endif}", interface )))
        
        if interface_is_up then
            result = result .. string.format( f(longstring), interface, gfontname, gfontsize )
        end
    end

    return result
end


function conky_get_processes(p_numb_proc)
    p_numb_proc = tonumber( p_numb_proc )
    local result = get_segment_header("PROCESSES")

    local processheader = string.format( f([[${font <gfontname>:style=Bold:size=<gfontsize>}Name ${alignr}  PID      CPU    MEM
${font <gfontname>:size=<gfontsize>}]]), gfontname, gfontsize )
    local longstring = [[${top name <row>} ${alignr}  ${top pid <row>}    ${top cpu <row>}    ${top mem <row>}]]

    result = result .. processheader

    for row = 1, p_numb_proc, 1 do
        if row < p_numb_proc then
            result = result .. string.format( f(longstring), row ) .. "\n"
        else
            result = result .. string.format( f(longstring), row )
        end
    end

    return result
end


function get_segment_header(p_segmentname)
    return string.format( f([[${color3}${hr}
    ${color2}${alignc}${font <gfontname>:bold:size=<gfontsize>}<p_segmentname> ${color}

]]), 
    gfontname, gfontsize, p_segmentname)
end


-- this is a helper method copied from 
-- https://hisham.hm/2016/01/04/string-interpolation-in-lua/
-- to support string interpolation
-- i've mod the code so that interpolation can be done by using <> instead of {}
function f(p_str)
    local outer_env = _ENV
    return (p_str:gsub("%b<>", function(block)
       local code = block:match("<(.*)>")
       local exp_env = {}
       setmetatable(exp_env, { __index = function(_, k)
          local stack_level = 5
          while debug.getinfo(stack_level, "") ~= nil do
             local i = 1
             repeat
                local name, value = debug.getlocal(stack_level, i)
                if name == k then
                   return value
                end
                i = i + 1
             until name == nil
             stack_level = stack_level + 1
          end
          return rawget(outer_env, k)
       end })
       local fn, err = load("return "..code, "expression `"..code.."`", "t", exp_env)
       if fn then
          return tostring(fn())
       else
          error(err, 0)
       end
    end))
end


-- this is a helper method copied from 
-- http://lua-users.org/wiki/SplitJoin
-- to support string split
function split(p_str, pat)
    local tbl = {}
    p_str:gsub(pat, function(x) tbl[#tbl+1]=x end)
    return tbl
end


function toboolean(p_str)
    if p_str == "true" or p_str == "True" then
        return true
    elseif p_str == "false" or p_str == "False" then
        return false
    else
        return nil
    end
end


function trimarraystring(p_str)
    p_str = p_str:gsub(" ", "")
    p_str = p_str:gsub("{", "")
    p_str = p_str:gsub("}", "")

    return p_str
end